export const environment = {
    production: true,
    api: 'https://elitex.nazarii-oliinyk.com/api/',
    site: 'https://elitex.nazarii-oliinyk.com/'
};
