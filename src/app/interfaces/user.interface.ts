export class User {
    id?: number;
    name?: string;
    stall_id?: number;
    email?: string;
    password?: string;
    token?: string;
}
