export interface ParkingEvent {
    name: string;
    user_name: string;
    stall_id: number;
    stall_status: boolean;
    created_at: number;
}
