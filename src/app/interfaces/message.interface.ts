export interface Message {
    name: string;
    text: string;
    date: number;
}
