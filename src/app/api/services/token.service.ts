import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TokenService {
    private iss = {
        login: environment.api + 'login',
        signup: environment.api + 'signup'
    };

    constructor() {
    }

    handle(token) {
        this.setToken(token);
    }

    setToken(token) {
        localStorage.setItem('token', token);
    }

    getToken() {
        return localStorage.getItem('token');
    }

    removeToken() {
        localStorage.removeItem('token');
    }

    isValid() {
        const token = this.getToken();
        if (token) {
            const payload = this.payload(token);
            if (payload) {
                return Object.values(this.iss).indexOf(payload.iss) > -1;
            }
        }
        return false;
    }

    payload(token) {
        const payload = token.split('.')[1];
        return this.decodePayload(payload);
    }

    decodePayload(payload) {
        return JSON.parse(atob(payload));
    }

    loggedIn() {
        return this.isValid();
    }
}
