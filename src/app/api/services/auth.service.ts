import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private httpClient: HttpClient) {
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    signUp(name: string, email: string, password: string): Observable<any> {
        return this.httpClient.post(environment.api + 'signup', {name, email, password});
    }

    logIn(email: string, password: string): Observable<any> {
        return this.httpClient.post(environment.api + 'login', {email, password});
    }

    isUserLogged(): Observable<boolean> {
        return of(!!localStorage.getItem('token'));
    }

    checkAuth(): Observable<any> {
        return this.httpClient.post(environment.api + 'check-auth', {'token': localStorage.getItem('token')});
    }
}
