import { TestBed } from '@angular/core/testing';

import { ParkingEventsService } from './parking-events.service';

describe('ParkingEventsService', () => {
  let service: ParkingEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParkingEventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
