import { TestBed } from '@angular/core/testing';

import { ParkingStallsService } from './parking-stalls.service';

describe('ParkingStallsService', () => {
  let service: ParkingStallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParkingStallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
