import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ParkingEvent} from '../../interfaces/parking-event.interface';

@Injectable({
    providedIn: 'root'
})
export class ParkingEventsService {

    constructor(private httpClient: HttpClient) {
    }

    getAllEvents(): Observable<ParkingEvent[]> {
        return this.httpClient.get<ParkingEvent[]>(environment.api + 'events');
    }
}
