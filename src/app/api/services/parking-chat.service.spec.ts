import { TestBed } from '@angular/core/testing';

import { ParkingChatService } from './parking-chat.service';

describe('ChatService', () => {
  let service: ParkingChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParkingChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
