import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Stalls} from '../../store/state/stalls.state';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ParkingStallsService {

    constructor(private httpClient: HttpClient) {
    }

    getAllStalls(stall_id: number, user_id: number): Observable<Stalls> {
        return this.httpClient.post<Stalls>(environment.api + 'stalls',{stall_id: stall_id, user_id: user_id});
    }

    takeStall(stall_id: number, user_id: number): Observable<Stalls> {
        return this.httpClient.put<Stalls>(environment.api + 'stalls/take', {stall_id: stall_id, user_id: user_id});
    }

    releaseStall(stall_id: number, user_id: number): Observable<Stalls> {
        return this.httpClient.put<Stalls>(environment.api + 'stalls/release', {stall_id: stall_id, user_id: user_id});
    }
}
