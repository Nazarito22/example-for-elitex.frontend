import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState} from '../../store/state/app.state';
import {Observable} from 'rxjs';
import {SignUp} from '../../store/actions/auth.actions';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    public signUpForm: FormGroup;
    public isSubmitted: boolean = false;
    public error: object = null;
    getState: Observable<any>;
    errorMessage: string | null;

    get f() {
        return this.signUpForm.controls;
    }

    constructor(private formBuilder: FormBuilder,
                private store: Store<AppState>) {
        this.getState = this.store.select(selectAuthState);
    }

    ngOnInit() {
        this.signUpForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
        });

        this.getState.subscribe((state) => {
            this.errorMessage = state.errorMessage;
        });
    }

    onSubmit(data) {
        this.isSubmitted = true;
        if (this.isSubmitted && this.signUpForm.valid) {
            const payload = {
                name: data.name,
                email: data.email,
                password: data.password
            };
            this.store.dispatch(new SignUp(payload));
            this.isSubmitted = false;
        }
    }

    handleError(error) {
        this.error = error.error.error;
    }
}
