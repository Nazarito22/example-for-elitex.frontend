import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LogIn} from '../../store/actions/auth.actions';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState} from '../../store/state/app.state';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public signInForm: FormGroup;
    public isSubmitted: boolean = false;
    public error: object = null;
    getState: Observable<any>;
    errorMessage: string | null;

    get f() {
        return this.signInForm.controls;
    }

    constructor(private formBuilder: FormBuilder,
                private store: Store<AppState>) {
        this.getState = this.store.select(selectAuthState);
    }

    ngOnInit() {
        this.signInForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
        });

        this.getState.subscribe((state) => {
            this.errorMessage = state.errorMessage;

        });
    }

    onSubmit(data) {
        this.isSubmitted = true;
        if (this.isSubmitted && this.signInForm.valid) {
            const payload = {
                email: data.email,
                password: data.password
            };
            this.store.dispatch(new LogIn(payload));
            this.isSubmitted = false;
        }
    }
}
