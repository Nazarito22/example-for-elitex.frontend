import {Component, OnInit, ViewChild} from '@angular/core';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ParkingChatService} from '../../../api/services/parking-chat.service';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState} from '../../../store/state/app.state';

@Component({
    selector: 'app-parking-chat',
    templateUrl: './parking-chat.component.html',
    styleUrls: ['./parking-chat.component.scss']
})
export class ParkingChatComponent implements OnInit {
    @ViewChild('perfectScroll') perfectScroll: PerfectScrollbarComponent;
    scrollConfig = {
        suppressScrollX: false,
        suppressScrollY: true
    };
    public messageForm: FormGroup;
    public isSubmitted = false;
    private userName: string;
    public messageArray: { user: string, message: string, date: number }[] = [];

    get f() {
        return this.messageForm.controls;
    }

    constructor(private formBuilder: FormBuilder,
                private parkingChatService: ParkingChatService,
                private store: Store<AppState>) {
        this.store.select(selectAuthState).subscribe(data => {
            this.userName = data.user.name;
            if (this.userName !== '') {
                this.join(this.userName, '0');
            }
        });
    }

    ngOnInit(): void {
        this.messageForm = this.formBuilder.group({
            message: ['', []]
        });

        this.parkingChatService.getMessage()
            .subscribe((data: { user: string, room: string, message: string, date: number }) => {
                this.messageArray.push(data);
                this.perfectScroll.directiveRef.update();
                this.perfectScroll.directiveRef.scrollToBottom(-200, 500);
            });
    }

    join(username: string, roomId: string): void {
        this.parkingChatService.joinRoom({user: username, room: roomId});
    }

    onSubmit(data) {
        this.isSubmitted = true;
        this.parkingChatService.sendMessage({
            user: this.userName,
            room: '0',
            message: data.message,
            date: new Date().getTime()
        });
        this.messageForm.controls.message.reset();
    }
}
