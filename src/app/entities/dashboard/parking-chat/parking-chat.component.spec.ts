import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ParkingChatComponent} from './parking-chat.component';

describe('ChatComponent', () => {
  let component: ParkingChatComponent;
  let fixture: ComponentFixture<ParkingChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
