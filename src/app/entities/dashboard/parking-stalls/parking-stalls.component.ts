import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState, selectStallsState} from '../../../store/state/app.state';
import {Stall} from '../../../store/state/stalls.state';
import {GetAllStalls} from '../../../store/actions/stalls.actions';
import {map, takeUntil} from 'rxjs/operators';
import {Actions, ofType} from '@ngrx/effects';
import {AuthActionTypes, CheckAuth} from '../../../store/actions/auth.actions';

@Component({
    selector: 'app-parking-stalls',
    templateUrl: './parking-stalls.component.html',
    styleUrls: ['./parking-stalls.component.scss']
})
export class ParkingStallsComponent implements OnInit, OnDestroy {
    public stalls$: Observable<Stall[]>;
    private user_id: number;
    private stall_id: number;
    private destroyed$ = new Subject<boolean>();

    constructor(private store: Store<AppState>,
                private actions: Actions) {
        this.actions.pipe(
            ofType(AuthActionTypes.CHECK_AUTH_SUCCESS, AuthActionTypes.LOGIN_SUCCESS, AuthActionTypes.SIGNUP_SUCCESS),
            takeUntil(this.destroyed$)
        ).subscribe(() => {
            this.store.select(selectAuthState).subscribe(auth => {
                this.user_id = auth.user.id;
                this.stall_id = auth.user.stall_id;
            });

            const payload = {
                user_id: this.user_id,
                stall_id: this.stall_id
            };

            this.store.dispatch(new GetAllStalls(payload));
        });
    }

    ngOnInit(): void {
        this.store.dispatch(new CheckAuth);
        this.stalls$ = this.store.select(selectStallsState).pipe(
            map(x => x.stalls)
        );
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}
