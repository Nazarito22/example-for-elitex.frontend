import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ParkingStallsComponent} from './parking-stalls.component';

describe('ParkingMonitorComponent', () => {
  let component: ParkingStallsComponent;
  let fixture: ComponentFixture<ParkingStallsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingStallsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingStallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
