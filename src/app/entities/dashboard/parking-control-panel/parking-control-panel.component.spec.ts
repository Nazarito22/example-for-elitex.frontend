import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingControlPanelComponent } from './parking-control-panel.component';

describe('ParkingControlPanelComponent', () => {
  let component: ParkingControlPanelComponent;
  let fixture: ComponentFixture<ParkingControlPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingControlPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingControlPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
