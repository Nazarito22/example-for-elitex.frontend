import {Component, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState, selectStallsState} from '../../../store/state/app.state';
import {Stall} from '../../../store/state/stalls.state';
import {Observable, Subject} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ReleaseStall, StallsActionTypes, TakeStall} from '../../../store/actions/stalls.actions';
import {map, takeUntil} from 'rxjs/operators';
import {Actions, ofType} from '@ngrx/effects';
import {UpdateStallId} from '../../../store/actions/auth.actions';

@Component({
    selector: 'app-parking-control-panel',
    templateUrl: './parking-control-panel.component.html',
    styleUrls: ['./parking-control-panel.component.scss']
})
export class ParkingControlPanelComponent implements OnDestroy {
    public stalls$: Observable<Stall[]>;
    public takeStallForm: FormGroup;
    public isSubmitted: boolean = false;
    public isUserTakeStall$: Observable<boolean>;
    public userCurrentStallNumber$: Observable<number>;
    private destroyed$ = new Subject<boolean>();
    private user_id: number;
    private stall_id: number;

    get f() {
        return this.takeStallForm.controls;
    }

    constructor(private formBuilder: FormBuilder,
                private store: Store<AppState>,
                private actions: Actions) {

        this.userCurrentStallNumber$ = this.store.select(selectAuthState).pipe(
            map(x => x.user.stall_id));
        this.isUserTakeStall$ = this.store.select(selectAuthState).pipe(
            map(x => x.user.stall_id == 0));

        this.actions.pipe(
            ofType(...[StallsActionTypes.GET_ALL_STALLS_SUCCESS, StallsActionTypes.TAKE_STALL_SUCCESS, StallsActionTypes.RELEASE_STALL_SUCCESS]),
            takeUntil(this.destroyed$)
        ).subscribe(payload => {
            this.store.dispatch(new UpdateStallId(payload));
        });
    }

    ngOnInit() {
        this.takeStallForm = this.formBuilder.group({
            stall_id: ['']
        });

        this.stalls$ = this.store.select(selectStallsState).pipe(
            map(x => x.stalls.filter(stall => stall.status === false))
        );
    }

    takeStall(data) {
        this.isSubmitted = true;
        if (this.isSubmitted) {
            this.store.select(selectAuthState).subscribe(auth => {
                this.user_id = auth.user.id;
            });
            const payload = {
                stall_id: data.stall_id,
                user_id: this.user_id
            };
            this.store.dispatch(new TakeStall(payload));
            this.isSubmitted = false;
        }
    }

    releaseStall() {
        this.store.select(selectAuthState).subscribe(auth => {
            this.user_id = auth.user.id;
            this.stall_id = auth.user.stall_id;
        });
        const payload = {
            stall_id: this.stall_id,
            user_id: this.user_id
        };
        this.store.dispatch(new ReleaseStall(payload));
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}
