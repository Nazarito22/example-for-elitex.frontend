import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {ParkingChatComponent} from './parking-chat/parking-chat.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ParkingStallsComponent} from './parking-stalls/parking-stalls.component';
import {ParkingStallEventsComponent} from './parking-stall-events/parking-stall-events.component';
import {ParkingControlPanelComponent} from './parking-control-panel/parking-control-panel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';

export const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    }
];

@NgModule({
    declarations: [DashboardComponent, ParkingChatComponent, ParkingStallsComponent, ParkingStallEventsComponent, ParkingControlPanelComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatCardModule,
        PerfectScrollbarModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatSelectModule
    ]
})
export class DashboardModule {
}
