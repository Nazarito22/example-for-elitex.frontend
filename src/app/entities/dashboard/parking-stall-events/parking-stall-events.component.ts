import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState, selectEventsState, selectStallsState} from '../../../store/state/app.state';
import {Store} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {StallsState} from '../../../store/state/stalls.state';
import {ParkingEvent} from '../../../interfaces/parking-event.interface';
import {map, takeUntil} from 'rxjs/operators';
import {GetAllEvents} from '../../../store/actions/events.actions';
import {Actions, ofType} from '@ngrx/effects';
import {StallsActionTypes} from '../../../store/actions/stalls.actions';

@Component({
    selector: 'app-parking-stall-events',
    templateUrl: './parking-stall-events.component.html',
    styleUrls: ['./parking-stall-events.component.scss']
})
export class ParkingStallEventsComponent implements OnInit, OnDestroy {
    public totalFreeStalls$: Observable<number | StallsState | AppState>;
    public parkingEvents$: Observable<ParkingEvent[]>;
    private destroyed$ = new Subject<boolean>();

    constructor(private store: Store<AppState>,
                private actions: Actions) {
        this.totalFreeStalls$ = this.store.select(selectStallsState).pipe(
            map(x => x.totalFreeStalls)
        );

        this.parkingEvents$ = this.store.select(selectEventsState).pipe(
            map(x => x.events)
        );

        this.actions.pipe(
            ofType(...[StallsActionTypes.GET_ALL_STALLS_SUCCESS, StallsActionTypes.TAKE_STALL_SUCCESS, StallsActionTypes.RELEASE_STALL_SUCCESS]),
            takeUntil(this.destroyed$)
        ).subscribe(() => {
            this.store.dispatch(new GetAllEvents);
        });
    }

    ngOnInit(): void {
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}
