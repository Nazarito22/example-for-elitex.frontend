import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingStallEventsComponent } from './parking-stall-events.component';

describe('ParkingStallEventsComponent', () => {
  let component: ParkingStallEventsComponent;
  let fixture: ComponentFixture<ParkingStallEventsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingStallEventsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingStallEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
