import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Router, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {TokenService} from '../api/services/token.service';
import {AppState} from '../store/state/app.state';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
    constructor(
        private router: Router,
        private tokenService: TokenService,
        private store: Store<AppState>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this.tokenService.getToken()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

    canLoad(): boolean {
        if (!this.tokenService.getToken()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}
