import {Component, OnDestroy, OnInit} from '@angular/core';
import {ParkingChatService} from './api/services/parking-chat.service';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from './store/state/app.state';
import {AuthService} from './api/services/auth.service';
import {Observable, Subject} from 'rxjs';
import {LogOut} from './store/actions/auth.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    autoRenew = new FormControl();
    public isAuthenticated$: Observable<boolean>;
    private isToken$: Observable<boolean>;
    public username$: Observable<string>;
    private destroyed$ = new Subject<boolean>();

    constructor(private chatService: ParkingChatService,
                private authService: AuthService,
                private store: Store<AppState>) {
        this.isAuthenticated$ = this.store.select(state => state.auth.isAuthenticated);
        this.username$ = this.store.select(state => state.auth.user.name);
    }

    ngOnInit() {
        this.isToken$ = this.authService.isUserLogged();
    }

    logout() {
        this.store.dispatch(new LogOut);
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}
