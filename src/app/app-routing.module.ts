import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './quards/auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./entities/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
    },
    {path: 'login', loadChildren: () => import('./entities/login/login.module').then(m => m.LoginModule)},
    {path: 'signup', loadChildren: () => import('./entities/signup/signup.module').then(m => m.SignupModule)},
    {path: '**', redirectTo: '/'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
