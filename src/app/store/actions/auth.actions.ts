import {Action} from '@ngrx/store';

export enum AuthActionTypes {
    LOGIN = '[Auth] Login',
    LOGIN_SUCCESS = '[Auth] Login Success',
    LOGIN_FAILURE = '[Auth] Login Failure',
    SIGNUP = '[Auth] Signup',
    SIGNUP_SUCCESS = '[Auth] Signup Success',
    SIGNUP_FAILURE = '[Auth] Signup Failure',
    LOGOUT = '[Auth] Logout',
    CHECK_AUTH = '[Auth] Check Auth',
    CHECK_AUTH_SUCCESS = '[Auth] Check Auth Success',
    CHECK_AUTH_FAILURE = '[Auth] Check Auth Failure',
    UPDATE_STALL_ID = '[User] Update Stall ID',
    UPDATE_STALL_ID_SUCCESS = '[User] Update Stall ID Success',
    UPDATE_STALL_ID_FAILURE = '[User] Update Stall ID Failure'
}

export class LogIn implements Action {
    readonly type = AuthActionTypes.LOGIN;

    constructor(public payload: any) {
    }
}

export class LogInSuccess implements Action {
    readonly type = AuthActionTypes.LOGIN_SUCCESS;

    constructor(public payload: any) {
    }
}

export class LogInFailure implements Action {
    readonly type = AuthActionTypes.LOGIN_FAILURE;

    constructor(public payload: any) {
    }
}

export class SignUp implements Action {
    readonly type = AuthActionTypes.SIGNUP;

    constructor(public payload: any) {
    }
}

export class SignUpSuccess implements Action {
    readonly type = AuthActionTypes.SIGNUP_SUCCESS;

    constructor(public payload: any) {
    }
}

export class SignUpFailure implements Action {
    readonly type = AuthActionTypes.SIGNUP_FAILURE;

    constructor(public payload: any) {
    }
}

export class LogOut implements Action {
    readonly type = AuthActionTypes.LOGOUT;
}

export class CheckAuth implements Action {
    readonly type = AuthActionTypes.CHECK_AUTH;
}

export class CheckAuthSuccess implements Action {
    readonly type = AuthActionTypes.CHECK_AUTH_SUCCESS;

    constructor(public payload: any) {
    }
}

export class CheckAuthFailure implements Action {
    readonly type = AuthActionTypes.CHECK_AUTH_FAILURE;

    constructor(public payload: any) {
    }
}

export class UpdateStallId implements Action {
    readonly type = AuthActionTypes.UPDATE_STALL_ID;

    constructor(public payload: any) {
    }
}

export class UpdateStallIdSuccess implements Action {
    readonly type = AuthActionTypes.UPDATE_STALL_ID_SUCCESS;

    constructor(public payload: any) {
    }
}

export class UpdateStallIdFailure implements Action {
    readonly type = AuthActionTypes.UPDATE_STALL_ID_FAILURE;

    constructor(public payload: any) {
    }
}

export type AllAuthActions =
    | LogIn
    | LogInSuccess
    | LogInFailure
    | SignUp
    | SignUpSuccess
    | SignUpFailure
    | LogOut
    | CheckAuth
    | CheckAuthSuccess
    | CheckAuthFailure
    | UpdateStallId
    | UpdateStallIdSuccess
    | UpdateStallIdFailure;
