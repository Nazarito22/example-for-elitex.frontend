import {Action} from '@ngrx/store';

export enum StallsActionTypes {
    GET_ALL_STALLS = '[Stalls] Get All Stalls',
    GET_ALL_STALLS_SUCCESS = '[Stalls] Get All Stalls Success',
    GET_ALL_STALLS_FAILURE = '[Stalls] Get All Stalls Failure',
    TAKE_STALL = '[Stalls] Take Parking Stall',
    TAKE_STALL_SUCCESS = '[Stalls] Take Parking Stall Success',
    TAKE_STALL_FAILURE = '[Stalls] Take Parking Stall Failure',
    RELEASE_STALL = '[Stalls] Release Parking Stall',
    RELEASE_STALL_SUCCESS = '[Stalls] Release Parking Stall Success',
    RELEASE_STALL_FAILURE = '[Stalls] Release Parking Stall Failure'
}

export class GetAllStalls implements Action {
    readonly type = StallsActionTypes.GET_ALL_STALLS;

    constructor(public payload: any) {
    }
}

export class GetAllStallsSuccess implements Action {
    readonly type = StallsActionTypes.GET_ALL_STALLS_SUCCESS;

    constructor(public payload: any) {
    }
}

export class GetAllStallsFailure implements Action {
    readonly type = StallsActionTypes.GET_ALL_STALLS_FAILURE;

    constructor(public payload: any) {
    }
}

export class TakeStall implements Action {
    readonly type = StallsActionTypes.TAKE_STALL;

    constructor(public payload: any) {
    }
}

export class TakeStallSuccess implements Action {
    readonly type = StallsActionTypes.TAKE_STALL_SUCCESS;

    constructor(public payload: any) {
    }
}

export class TakeStallFailure implements Action {
    readonly type = StallsActionTypes.TAKE_STALL_FAILURE;

    constructor(public payload: any) {
    }
}

export class ReleaseStall implements Action {
    readonly type = StallsActionTypes.RELEASE_STALL;

    constructor(public payload: any) {
    }
}

export class ReleaseStallSuccess implements Action {
    readonly type = StallsActionTypes.RELEASE_STALL_SUCCESS;

    constructor(public payload: any) {
    }
}

export class ReleaseStallFailure implements Action {
    readonly type = StallsActionTypes.RELEASE_STALL_FAILURE;

    constructor(public payload: any) {
    }
}

export type AllStallsActions =
    | GetAllStalls
    | GetAllStallsSuccess
    | GetAllStallsFailure
    | TakeStall
    | TakeStallSuccess
    | TakeStallFailure
    | ReleaseStall
    | ReleaseStallSuccess
    | ReleaseStallFailure;
