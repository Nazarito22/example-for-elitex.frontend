import {Action} from '@ngrx/store';

export enum EventsActionTypes {
    GET_ALL_EVENTS = '[Events] Get All Events',
    GET_ALL_EVENTS_SUCCESS = '[Events] Get All Events Success',
    GET_ALL_EVENTS_FAILURE = '[Events] Get All Events Failure'
}

export class GetAllEvents implements Action {
    readonly type = EventsActionTypes.GET_ALL_EVENTS;

    constructor() {
    }
}

export class GetAllEventsSuccess implements Action {
    readonly type = EventsActionTypes.GET_ALL_EVENTS_SUCCESS;

    constructor(public payload: any) {
    }
}

export class GetAllEventsFailure implements Action {
    readonly type = EventsActionTypes.GET_ALL_EVENTS_FAILURE;

    constructor(public payload: any) {
    }
}

export type AllEventsActions =
    | GetAllEvents
    | GetAllEventsSuccess
    | GetAllEventsFailure;

