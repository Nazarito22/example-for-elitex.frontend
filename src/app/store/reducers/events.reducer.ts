import {AllEventsActions, EventsActionTypes} from '../actions/events.actions';
import {EventsState, initialEventsState} from '../state/events.state';

export function eventReducer(state = initialEventsState, action: AllEventsActions): EventsState {
    switch (action.type) {
        case EventsActionTypes.GET_ALL_EVENTS_SUCCESS: {
            return {
                ...state,
                events: action.payload.events,
                errorMessage: null
            };
        }
        case EventsActionTypes.GET_ALL_EVENTS_FAILURE: {
            return {
                ...state,
                errorMessage: 'Something went wrong.'
            };
        }
        default: {
            return state;
        }
    }
}
