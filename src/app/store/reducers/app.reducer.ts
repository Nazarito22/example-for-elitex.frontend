import {ActionReducerMap} from '@ngrx/store';
import {routerReducer} from '@ngrx/router-store';
import {stallReducer} from './stalls.reducer';
import {AppState} from '../state/app.state';
import {authReducer} from './auth.reducer';
import {eventReducer} from './events.reducer';

export const appReducers: ActionReducerMap<AppState, any> = {
    router: routerReducer,
    parking_stalls: stallReducer,
    events: eventReducer,
    auth: authReducer
};
