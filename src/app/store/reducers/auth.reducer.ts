import {AllAuthActions, AuthActionTypes} from '../actions/auth.actions';
import {AuthState, initialAuthState} from '../state/auth.state';

export function authReducer(state = initialAuthState, action: AllAuthActions): AuthState {
    switch (action.type) {
        case AuthActionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    id: action.payload.id,
                    name: action.payload.name,
                    stall_id: action.payload.stall_id,
                    token: action.payload.token
                },
                errorMessage: null
            };
        }
        case AuthActionTypes.LOGIN_FAILURE: {
            return {
                ...state,
                errorMessage: 'Incorrect email and/or password.'
            };
        }
        case AuthActionTypes.SIGNUP_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    id: action.payload.id,
                    name: action.payload.name,
                    stall_id: action.payload.stall_id,
                    token: action.payload.token
                },
                errorMessage: null
            };
        }
        case AuthActionTypes.SIGNUP_FAILURE: {
            return {
                ...state,
                errorMessage: 'That email is already in use.'
            };
        }
        case AuthActionTypes.LOGOUT: {
            return initialAuthState;
        }
        case AuthActionTypes.CHECK_AUTH_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    id: action.payload.id,
                    name: action.payload.name,
                    stall_id: action.payload.stall_id,
                    token: action.payload.token
                },
                errorMessage: null
            };
        }
        case AuthActionTypes.CHECK_AUTH_FAILURE: {
            return {
                ...state,
                errorMessage: 'That email is already in use.'
            };
        }
        case AuthActionTypes.UPDATE_STALL_ID_SUCCESS: {
            return {
                ...state,
                isAuthenticated: state.isAuthenticated,
                user: {
                    id: state.user.id,
                    name: state.user.name,
                    stall_id: action.payload.stall_id,
                    token: state.user.token
                },
                errorMessage: state.errorMessage
            };
        }
        case AuthActionTypes.UPDATE_STALL_ID_FAILURE: {
            return {
                ...state,
                errorMessage: 'Stall ID for user not changed.'
            };
        }
        default: {
            return state;
        }
    }
}
