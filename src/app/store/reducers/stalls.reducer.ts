import {initialStallsState, StallsState} from '../state/stalls.state';
import {AllStallsActions, StallsActionTypes} from '../actions/stalls.actions';

export function stallReducer(state = initialStallsState, action: AllStallsActions): StallsState {
    switch (action.type) {
        case StallsActionTypes.GET_ALL_STALLS_SUCCESS: {
            return {
                ...state,
                totalFreeStalls: action.payload.totalFreeStalls,
                stalls: action.payload.stalls,
                errorMessage: null
            };
        }
        case StallsActionTypes.GET_ALL_STALLS_FAILURE: {
            return {
                ...state,
                errorMessage: 'Something went wrong.'
            };
        }

        case StallsActionTypes.TAKE_STALL_SUCCESS: {
            return {
                ...state,
                totalFreeStalls: action.payload.totalFreeStalls,
                stalls: action.payload.stalls,
                errorMessage: null
            };
        }
        case StallsActionTypes.TAKE_STALL_FAILURE: {
            return {
                ...state,
                errorMessage: 'Something went wrong.'
            };
        }

        case StallsActionTypes.RELEASE_STALL_SUCCESS: {
            return {
                ...state,
                totalFreeStalls: action.payload.totalFreeStalls,
                stalls: action.payload.stalls,
                errorMessage: null
            };
        }
        case StallsActionTypes.RELEASE_STALL_FAILURE: {
            return {
                ...state,
                errorMessage: 'Something went wrong.'
            };
        }
        default: {
            return state;
        }
    }
}

