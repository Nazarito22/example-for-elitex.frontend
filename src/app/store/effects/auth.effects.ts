import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {
    AuthActionTypes,
    CheckAuthFailure,
    CheckAuthSuccess,
    LogIn,
    LogInFailure,
    LogInSuccess,
    SignUp,
    SignUpFailure,
    SignUpSuccess,
    UpdateStallId,
    UpdateStallIdFailure,
    UpdateStallIdSuccess,
} from '../actions/auth.actions';
import {AuthService} from '../../api/services/auth.service';

@Injectable()
export class AuthEffects {
    constructor(
        private actions: Actions,
        private authService: AuthService,
        private router: Router,
    ) {
    }

    LogIn: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.LOGIN),
        map((action: LogIn) => action.payload),
        switchMap(payload => {
            return this.authService.logIn(payload.email, payload.password).pipe(
                map((user) => {
                    return new LogInSuccess({
                        id: user.id,
                        name: user.name,
                        stall_id: user.stall_id,
                        token: user.access_token
                    });
                }),
                catchError((error) => {
                    return of(new LogInFailure({error: error}));
                })
            );
        })), {dispatch: true});

    LogInSuccess: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_SUCCESS),
        map((action: LogIn) => action.payload),
        tap((user) => {
            localStorage.setItem('token', user.token);
            this.router.navigateByUrl('/');
        })
    ), {dispatch: false});


    LogInFailure: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_FAILURE)
    ), {dispatch: false});


    SignUp: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.SIGNUP),
        map((action: SignUp) => action.payload),
        switchMap(payload => {
            return this.authService.signUp(payload.name, payload.email, payload.password).pipe(
                map((user) => {
                    return new SignUpSuccess({
                        id: user.id,
                        name: user.name,
                        stall_id: user.stall_id,
                        token: user.access_token
                    });
                }),
                catchError((error) => {
                    return of(new SignUpFailure({error: error}));
                })
            );
        })
    ), {dispatch: true});


    SignUpSuccess: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.SIGNUP_SUCCESS),
        map((action: SignUp) => action.payload),
        tap((user) => {
            localStorage.setItem('token', user.token);
            this.router.navigateByUrl('/');
        })
    ), {dispatch: false});


    SignUpFailure: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.SIGNUP_FAILURE)
    ), {dispatch: false});


    public LogOut: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.LOGOUT),
        tap((user) => {
            localStorage.removeItem('token');
            this.router.navigateByUrl('/login');
        })
    ), {dispatch: false});

    CheckAuth: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.CHECK_AUTH),
        switchMap(payload => {
            return this.authService.checkAuth().pipe(
                map((user) => {
                    return new CheckAuthSuccess({
                        id: user.id,
                        name: user.name,
                        stall_id: user.stall_id,
                        token: user.access_token
                    });
                }),
                catchError((error) => {
                    return of(new CheckAuthFailure({error: error}));
                })
            );
        })), {dispatch: true});

    public UpdateStallId: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.UPDATE_STALL_ID),
        map((action: UpdateStallId) => action.payload),
        map(payload => {
            return new UpdateStallIdSuccess({
                stall_id: payload.payload.currentStallForUser
            });
        }),
        catchError((error) => {
            return of(new UpdateStallIdFailure({error: error}));
        })
    ), {dispatch: true});
}
