import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ParkingEventsService} from '../../api/services/parking-events.service';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {EventsActionTypes, GetAllEventsFailure, GetAllEventsSuccess} from '../actions/events.actions';

@Injectable()
export class EventsEffects {
    constructor(
        private actions: Actions,
        private parkingEventsService: ParkingEventsService
    ) {
    }

    GetAllEvents: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(EventsActionTypes.GET_ALL_EVENTS),
        switchMap(payload => {
            return this.parkingEventsService.getAllEvents().pipe(
                map((events) => {
                    return new GetAllEventsSuccess(
                        {
                            events: events
                        });
                }),
                catchError((error) => {
                    return of(new GetAllEventsFailure({error: error}));
                })
            );
        })), {dispatch: true});

}
