import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ParkingStallsService} from '../../api/services/parking-stalls.service';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {
    GetAllStalls,
    GetAllStallsFailure,
    GetAllStallsSuccess,
    ReleaseStallFailure,
    ReleaseStallSuccess,
    StallsActionTypes,
    TakeStall,
    TakeStallFailure,
    TakeStallSuccess
} from '../actions/stalls.actions';

@Injectable()
export class StallsEffects {
    constructor(
        private actions: Actions,
        private parkingStallsService: ParkingStallsService
    ) {
    }

    GetAllStalls: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(StallsActionTypes.GET_ALL_STALLS),
        map((action: GetAllStalls) => action.payload),
        switchMap(payload => {
            return this.parkingStallsService.getAllStalls(payload.stall_id, payload.user_id).pipe(
                map((data) => {
                    return new GetAllStallsSuccess(
                        {
                            currentStallForUser: data.currentStallForUser,
                            totalFreeStalls: data.stalls.filter(stall => stall.status === false).length,
                            stalls: data.stalls
                        });
                }),
                catchError((error) => {
                    return of(new GetAllStallsFailure({error: error}));
                })
            );
        })), {dispatch: true});

    TakeStall: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(StallsActionTypes.TAKE_STALL),
        map((action: TakeStall) => action.payload),
        switchMap(payload => {
            return this.parkingStallsService.takeStall(payload.stall_id, payload.user_id).pipe(
                map((data) => {
                    return new TakeStallSuccess(
                        {
                            currentStallForUser: data.currentStallForUser,
                            totalFreeStalls: data.stalls.filter(stall => stall.status === false).length,
                            stalls: data.stalls
                        });
                }),
                catchError((error) => {
                    return of(new TakeStallFailure({error: error}));
                })
            );
        })), {dispatch: true});

    ReleaseStall: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(StallsActionTypes.RELEASE_STALL),
        map((action: TakeStall) => action.payload),
        switchMap(payload => {
            return this.parkingStallsService.releaseStall(payload.stall_id, payload.user_id).pipe(
                map((data) => {
                    return new ReleaseStallSuccess(
                        {
                            currentStallForUser: data.currentStallForUser,
                            totalFreeStalls: data.stalls.filter(stall => stall.status === false).length,
                            stalls: data.stalls
                        });
                }),
                catchError((error) => {
                    return of(new ReleaseStallFailure({error: error}));
                })
            );
        })), {dispatch: true});
}
