export interface Stall {
    id: number;
    user_id: number;
    status: boolean;
}

export interface Stalls {
    currentStallForUser: number;
    stalls: Stall[];
}

export interface StallsState {
    totalFreeStalls: number;
    stalls: Stall[];
    errorMessage: string | null;
}

export const initialStallsState: StallsState = {
    totalFreeStalls: 9,
    stalls: [
        {id: 1, user_id: 0, status: false},
        {id: 2, user_id: 0, status: false},
        {id: 3, user_id: 0, status: false},
        {id: 4, user_id: 0, status: false},
        {id: 5, user_id: 0, status: false},
        {id: 6, user_id: 0, status: false},
        {id: 7, user_id: 0, status: false},
        {id: 8, user_id: 0, status: false},
        {id: 9, user_id: 0, status: false},
    ],
    errorMessage: null
};
