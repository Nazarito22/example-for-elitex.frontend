import {ParkingEvent} from '../../interfaces/parking-event.interface';

export interface EventsState {
    events: ParkingEvent[];
    errorMessage: string | null;
}

export const initialEventsState: EventsState = {
    events: [],
    errorMessage: null
};
