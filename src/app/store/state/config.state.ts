export interface ConfigState {
    config: object
}

export const initialConfigState: ConfigState = {
    config: null
};
