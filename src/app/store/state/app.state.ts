import {initialStallsState, StallsState} from './stalls.state';
import {RouterReducerState} from '@ngrx/router-store';
import {AuthState, initialAuthState} from './auth.state';
import {createFeatureSelector} from '@ngrx/store';
import {EventsState, initialEventsState} from './events.state';

export interface AppState {
    router?: RouterReducerState,
    parking_stalls: StallsState,
    events: EventsState,
    auth: AuthState
}

export const initialAppState: AppState = {
    parking_stalls: initialStallsState,
    events: initialEventsState,
    auth: initialAuthState
};

export function initialState(): AppState {
    return initialAppState;
}

export const selectAuthState = createFeatureSelector<AuthState>('auth');
export const selectStallsState = createFeatureSelector<StallsState>('parking_stalls');
export const selectEventsState = createFeatureSelector<EventsState>('events');
