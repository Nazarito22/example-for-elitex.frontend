import {User} from '../../interfaces/user.interface';

export interface AuthState {
    isAuthenticated: boolean;
    user: User | null;
    errorMessage: string | null;
}

export const initialAuthState: AuthState = {
    isAuthenticated: false,
    user: {
        id: 0,
        name: '',
        stall_id: 0,
        email: '',
        password: '',
        token: ''
    },
    errorMessage: null
};

export function initialState(): AuthState {
    return initialAuthState;
}
